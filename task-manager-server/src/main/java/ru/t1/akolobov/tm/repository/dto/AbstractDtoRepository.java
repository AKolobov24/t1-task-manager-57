package ru.t1.akolobov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.api.repository.dto.IDtoRepository;
import ru.t1.akolobov.tm.dto.model.AbstractDtoModel;

import javax.persistence.EntityManager;
import java.util.Collection;

@Repository
@Scope("prototype")
public abstract class AbstractDtoRepository<M extends AbstractDtoModel> implements IDtoRepository<M> {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    public AbstractDtoRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) {
        models.forEach(this::add);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
