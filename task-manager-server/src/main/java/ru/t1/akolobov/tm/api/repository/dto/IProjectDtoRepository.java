package ru.t1.akolobov.tm.api.repository.dto;

import ru.t1.akolobov.tm.dto.model.ProjectDto;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDto> {

}
