package ru.t1.akolobov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.akolobov.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.akolobov.tm.dto.model.AbstractUserOwnedDtoModel;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedDtoModel>
        extends AbstractDtoService<M> implements IUserOwnedDtoService<M> {

    @NotNull
    @Override
    protected abstract IUserOwnedDtoRepository<M> getRepository();

    @Override
    public void add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.existById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(final @Nullable String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId, sort);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
