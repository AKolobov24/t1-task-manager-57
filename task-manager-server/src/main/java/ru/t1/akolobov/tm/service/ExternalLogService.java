package ru.t1.akolobov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.api.service.IExternalLogService;
import ru.t1.akolobov.tm.dto.OperationEvent;

import javax.jms.*;
import javax.persistence.Table;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Setter
@Service
public final class ExternalLogService implements IExternalLogService {

    @NotNull
    private PropertyService propertyService;

    @NotNull
    private LoggerService loggerService;

    @Nullable
    private ConnectionFactory connectionFactory;

    private Connection connection;

    private Session session;

    private MessageProducer messageProducer;

    private boolean isDefined = true;

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

    public ExternalLogService(
            @NotNull @Autowired final ApplicationContext context,
            @NotNull @Autowired final LoggerService loggerService,
            @NotNull @Autowired final PropertyService propertyService
    ) {
        this.loggerService = loggerService;
        this.propertyService = propertyService;
        try {
            this.connectionFactory = context.getBean(ConnectionFactory.class);
        } catch (Exception e) {
            loggerService.error(e);
            isDefined = false;
        }
        define();
        if (!isDefined)
            loggerService.info("ERROR: ExternalLogService was not initialized due to errors!");
    }

    @SneakyThrows
    private void define() {
        if (!isDefined || connectionFactory == null) return;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            @NotNull final Queue destination = session.createQueue(propertyService.getExternalLoggerQueue());
            messageProducer = session.createProducer(destination);
            loggerService.info("External logger was initialized!");
        } catch (Exception e) {
            isDefined = false;
            loggerService.error(e);
        }
    }

    @SneakyThrows
    public void send(@NotNull final String text) {
        final TextMessage message = session.createTextMessage(text);
        messageProducer.send(message);
    }

    @Override
    public void send(@NotNull final OperationEvent event) {
        executorService.submit(() -> sync(event));
    }

    @SneakyThrows
    public void sync(@NotNull final OperationEvent event) {
        @NotNull final Class<?> entityClass = event.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            @NotNull final Table table = entityClass.getAnnotation(Table.class);
            event.setTable(table.name());
        }
        send(objectWriter.writeValueAsString(event));
    }

    @Override
    @SneakyThrows
    public void stop() {
        executorService.shutdown();
        if (!isDefined) return;
        session.close();
        connection.close();
    }

    @Override
    public boolean isDefined() {
        return isDefined;
    }

}
