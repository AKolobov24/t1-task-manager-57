package ru.t1.akolobov.tm.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
public final class ConsoleEvent {

    @NotNull
    private final String name;

}
