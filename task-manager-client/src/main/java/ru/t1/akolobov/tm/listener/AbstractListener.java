package ru.t1.akolobov.tm.listener;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.api.model.IListener;
import ru.t1.akolobov.tm.api.service.ITokenService;
import ru.t1.akolobov.tm.event.ConsoleEvent;

@Getter
@Setter
@Component
public abstract class AbstractListener implements IListener {

    @NotNull
    @Autowired
    ITokenService tokenService;

    @Override
    @NotNull
    public abstract String getEventName();

    @Override
    @Nullable
    public abstract String getArgument();

    @Override
    @NotNull
    public abstract String getDescription();

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    @Override
    public abstract void handleEvent(@NotNull final ConsoleEvent event);

    @Override
    @NotNull
    public String toString() {
        @NotNull final String name = getEventName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        @NotNull String result = "";
        if (!name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (!description.isEmpty()) result += " : " + description;
        return result;
    }

}