package ru.t1.akolobov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.request.DataJsonSaveFasterXmlRequest;
import ru.t1.akolobov.tm.event.ConsoleEvent;

@Component
public final class DataJsonSaveFasterXmlListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-json";

    @NotNull
    public static final String DESCRIPTION = "Save data to json file.";

    @Override
    @NotNull
    public String getEventName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonSaveFasterXmlListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA SAVE JSON]");
        getDomainEndpoint().saveDataJsonFasterXml(new DataJsonSaveFasterXmlRequest(getToken()));
    }

}
