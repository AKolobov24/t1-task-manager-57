package ru.t1.akolobov.tm.exception.user;

public final class EmailExistException extends AbstractUserException {

    public EmailExistException() {
        super("Error! Email is already exists...");
    }

}
