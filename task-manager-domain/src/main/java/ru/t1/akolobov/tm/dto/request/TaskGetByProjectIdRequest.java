package ru.t1.akolobov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class TaskGetByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskGetByProjectIdRequest(@Nullable String token) {
        super(token);
    }

}
